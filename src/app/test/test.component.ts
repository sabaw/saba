import { Component, OnInit,Input,Output } from '@angular/core';



@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  class1 = "input";
  click =0;
  edit = 0;
  cancel = 0;
  id = 3;
  row : any = '';
  backUp=[];
  itIsClickRow =0;
 users = [{id : 1 ,email:'majid@veresk.org',name:'Majid',family:'Nayyeri'},
 {id : 2 , email:'m.a@veresk.org',name:'Mohsen',family:'Ashoori'}];
 user = {email :"",name :"",family :""};
 editUsers : any= {};
  constructor() { 
    
  }
  onAddUserClick(){
    this.click = 1;
    this.cancel=1;
    

  }
 onCloseClick(){
   this.click=0;
   this.cancel=0;
   this.user.email="" ;
   this.user.name="" ;
   this.user.family="" ;
   

  }
 onAddRow(){
 
  if(this.user.email!=""||this.user.name!=""||this.user.family!=""){
    this.users.push({ id : this.id,email:this.user.email,name:this.user.name,family:this.user.family});
  }
  this.user.email="" ;
  this.user.name="" ;
  this.user.family="" ;
  this.id+=1;
  
}
 onClickRow(val){
  
  this.edit = 1;
  if(this.itIsClickRow==0){
    this.backUp=JSON.parse(JSON.stringify(this.users));
    this.itIsClickRow=1;
    this.row = val.id;
  }
   
    if(val.id!=this.row){
      this.users=JSON.parse(JSON.stringify(this.backUp))
      this.itIsClickRow=0
      this.row = val.id;
    }
  
   
  
}
onCancelClick(){
this.edit=0;
this.cancel = 0;
this.click = 0;
if(this.itIsClickRow!=0)
  this.users=this.backUp;




}

onDeleteClick(){
  for (let i = 0; i < this.users.length; i++){  
    if(this.users[i].id==this.row)
      this.users.splice(i,1);
       this.edit=0;
       this.backUp=[];
  }
  this.cancel = 0;
  this.click = 0;
} 

onUpdateClick(){
    this.editUsers = {};
  this.edit=0;
  this.cancel = 0;
  this.click = 0;
 
   }

  ngOnInit() {
  }

}
