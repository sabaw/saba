import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {CategorisService} from './services/categoris.service'
import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import {FormsModule} from '@angular/forms';
@NgModule({
  declarations: [
    AppComponent,
    TestComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [CategorisService],
  bootstrap: [AppComponent],
  
})
export class AppModule {
 }
